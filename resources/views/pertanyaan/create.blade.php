@extends('adminlte.master')
@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create New Pertanyaan</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="judul">
                        Judul
                    </label>
                    <input type="judul" class="form-control" id="judul" name="judul" placeholder="Masukan Judul " value="{{old('judul','')}}">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class=" form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukan Isi" value="{{old('isi','')}}">
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tanggal_dibuat">Tanggal Dibuat</label>
                    <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat" value="{{old('tanggal_dibuat','')}}">
                    @error('tanggal_dibuat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tanggal_diperbaharui">Tanggal Diperbaharui</label>
                    <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui" value="{{old('tanggal_diperbaharui','')}}">
                    @error('tanggal_diperbaharui')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
</div>
@endsection