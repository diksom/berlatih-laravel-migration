<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            "judul" => 'required',
            "isi" => 'required',
            "tanggal_dibuat" => 'required',
            "tanggal_diperbaharui" => 'required'
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "tanggal_dibuat" => $request["tanggal_dibuat"],
        //     "tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
        // ]);
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->tanggal_dibuat = $request["tanggal_dibuat"];
        // $pertanyaan->tanggal_diperbaharui = $request["tanggal_diperbaharui"];
        // $pertanyaan->save();
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $request["tanggal_dibuat"],
            "tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
        ]);
        return redirect('/pertanyaan')->with('success', "Input Pertanyaan Berhasil");
    }
    public function index()
    {
        // $pertanyaan = DB::table('pertanyaan')->select('id', 'judul', 'isi', 'tanggal_dibuat', 'tanggal_diperbaharui')->get();
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function show($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            "judul" => 'required',
            "isi" => 'required',
            "tanggal_dibuat" => 'required',
            "tanggal_diperbaharui" => 'required'
        ]);

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi'],
        //         'tanggal_dibuat' => $request['tanggal_dibuat'],
        //         'tanggal_diperbaharui' => $request['tanggal_diperbaharui']
        //     ]);
        $pertanyaan = Pertanyaan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $request["tanggal_dibuat"],
            "tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    }
    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus');
    }
}
